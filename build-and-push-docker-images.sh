#!/bin/bash
set -e

docker build \
-t registry.gitlab.com/pleio/semantic-mediawiki:latest \
-t registry.gitlab.com/pleio/semantic-mediawiki:$(git rev-parse --short HEAD) \
.

docker push registry.gitlab.com/pleio/semantic-mediawiki:latest
docker push registry.gitlab.com/pleio/semantic-mediawiki:$(git rev-parse --short HEAD)
