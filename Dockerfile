FROM php:7-apache

# Request system packages
RUN apt-get update && apt-get install --no-install-recommends -y \
    libpng-dev \
    libjpeg-dev \
    wget \
    git \
    unzip \
    msmtp \
    diffutils \
    msmtp \
    gettext \
    imagemagick

RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && rm -rf /var/lib/apt/lists/*

# PHP extensions
RUN docker-php-ext-install mysqli gd

# PHP configuration
COPY ./docker/php.ini /usr/local/etc/php/php.ini

# Install Composer
RUN wget wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet && \
    mv composer.phar /usr/local/bin/composer
ENV COMPOSER_HOME=/tmp/composer

#  Scripts
COPY ./docker/start.sh /start.sh
COPY ./docker/initialize.sh /initialize.sh
RUN chmod +x /start.sh /initialize.sh

# Get Mediawiki
RUN wget https://releases.wikimedia.org/mediawiki/1.33/mediawiki-1.33.1.tar.gz -O temp.tar.gz && \
    mkdir /build && \
    tar -xzf temp.tar.gz -C /build && \
    mv /build/mediawiki-1.33.1 /app && \
    rm temp.tar.gz && \
    rm -Rf /build

# Get extensions
RUN wget https://extdist.wmflabs.org/dist/extensions/ConfirmAccount-REL1_33-cacb682.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/extensions/MsCatSelect-REL1_33-d63d5fc.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/extensions/MobileFrontend-REL1_33-312e8ed.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/extensions/PageForms-REL1_33-fe9dcc2.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz

RUN wget https://github.com/DaSchTour/matomo-mediawiki-extension/archive/v4.0.1.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/extensions && \
    rm temp.tar.gz && \
    mv /app/extensions/matomo-mediawiki-extension-4.0.1 /app/extensions/Matomo

# Get skins
RUN wget https://extdist.wmflabs.org/dist/skins/MinervaNeue-REL1_33-1903d1a.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/skins && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/skins/CologneBlue-REL1_33-73f7254.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/skins && \
    rm temp.tar.gz

RUN wget https://extdist.wmflabs.org/dist/skins/Modern-REL1_33-7bb4560.tar.gz -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/skins && \
    rm temp.tar.gz

# Additional configuration
COPY ./docker/000-default.conf /etc/apache2/sites-available
COPY ./docker/msmtp.conf /app/docker/msmtp.conf
COPY ./docker/remoteip.conf /etc/apache2/conf-available/remoteip.conf

# Apache modules
RUN a2enmod rewrite remoteip
RUN a2enconf remoteip

WORKDIR /app

# Create images symlink to storage
RUN rm -Rf /app/images && \
    ln -s /storage/images /app/images

# Install Mediawiki packages
RUN composer require --update-no-dev \
    mediawiki/semantic-media-wiki "3.1.6" \
    mediawiki/maps "7.18.0" \
    mediawiki/image-map "dev-REL1_33"

RUN apt-get update && apt-get remove -y \
    libpng-dev \
    libjpeg-dev \
    linux-libc-dev

COPY ./docker/LocalSettings.php /app
COPY ./assets /app/assets

# Define run script
CMD ["/start.sh"]
